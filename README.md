# ig-key-nav

A UserScript (for GreaseMonkey/TamperMonkey/VioletMonkey, etc) that adds some keyboard navigation
to Instagram.  It's relatively fragile as it encodes selectors using IG's variable class names. 

Since I first wrote this back in like 2020, IG has actually added some of its own, sometimes conflicting,
keyboard shortcuts.

Shortcuts:

- navigation
  - w or k: up (previous post)
  - s or j: down (next post)
  - a: previous image in a set within a single post
  - d: next image in a set within a single post
- actions
  - l: like the post/story
  - m: show more in a post's caption (if truncated)
  - b: bookmark a post
  - c: comment on a post

Copyright: Richard Schwarting © 2023

License: GPLv3+
