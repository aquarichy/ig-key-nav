// ==UserScript==
// @name     Instagram Keyboard Navigation
// @version  2
// @grant    none
// @match    https://www.instagram.com/*
// ==/UserScript==

/*
   Copyright: Richard Schwarting ® 2024
   License: GPLv3+
   Source: https://gitlab.com/aquarichy/ig-key-nav

   == Usage ==

   Allows you to use keyboard controls.

   w or k: up (previous post)
   s or j: down (next post)
   a: previous image in a set within a single post
   d: next image in a set within a single post

   l: like the post
   m: show more in the caption (if truncated)
   b: bookmark
   c: comment

   ==Error debugging==

   Exceptions caught by GreaseMonkey won't show up in a tab's console; need to
   check the browser console.  E.g. in Firefox, first, open a tab's console, and then press
   ctrl-shift-j to pop open the browser console.

   ==Installation==

   Add as a script to a UserScript extension like GreaseMonkey.
 */
const BUTTON_SEL_MAP = {
  /* d: view next image in image set post (wasd controls) */
  "d": "button._afxw",
  /* a: view prev image in image set post (wasd controls) */
  "a": "button._afxv",

  /* l: Like the selected article */
  "l": [
    /* /p/ (post), feed */
    "SECTION > MAIN DIV.x1i10hfl.x972fbf.xcfux6l.x1qhh985.xm0m39n.x9f619.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.x16tdsg8.x1hl2dhg.xggy1nq.x1a2a7pz.x6s0dn4.xjbqb8w.x1ejq31n.xd10rxx.x1sy0etr.x17r0tee.x1ypdohk.x78zum5.xl56j7k.x1y1aw1k.x1sxyh0.xwib8y2.xurb0ha.xcdnw81", // 2024-02-02

    /* /stories/ */
    "SECTION._ac0a DIV:has(> DIV > SPAN > svg[aria-label='Like'])",   // 2023-07-26
    "SECTION._ac0a DIV:has(> DIV > SPAN > svg[aria-label='Unlike'])", // 2023-07-26
  ].join (", "),
  /* m: show more of the caption if truncated */
  "m": "span._aacl._aaco._aacu._aacx._aad7._aade > span:nth-child(2) > div",
  /* b: bookmark */
  "b": "span._aamz > div > div > button._abl-",
};
const FOCUS_SEL_MAP = {
  /* c: comment */
  "c": "TEXTAREA._ablz._aaoc, " +
    "TEXTAREA.x1i0vuye.xvbhtw8.x76ihet.xwmqs3e.x112ta8.xxxdfa6.x5n08af.x78zum5.x1iyjqo2.x1qlqyl8.x1d6elog.xlk1fp6.x1a2a7pz.xexx8yu.x4uap5.x18d9i69.xkhd6sd.xtt52l0.xnalus7.x1bq4at4.xaqnwrm.xs3hnx8",
};

function get_sibling (article, next) {
  console.debug (`get_sibling: entered`, article, next);
  const sibling_prop = next ? "nextElementSibling" : "previousElementSibling";
  const sibling = article[sibling_prop]; /* may be null */
  const sibling_next = sibling[sibling_prop] ? sibling[sibling_prop] : null;
  const parent = article.parentElement;

  if (sibling && sibling.tagName == "ARTICLE") {
    console.debug (`get_sibling: article[${sibling_prop}] is another ARTICLE, returning`, sibling);
    return sibling;
  } else if (sibling && sibling.tagName == "DIV" && sibling_next && sibling_next.tagName == "ARTICLE") {
    /* skipping over Threads highlights */
    return sibling_next;
  } else if (parent.tagName == "DIV" &&
             parent.childElementCount == 1 &&
             parent[sibling_prop].tagName == "DIV") {
    sibling_div = parent[sibling_prop];

    for (let i = 0; i < 2; i++) {
      if (sibling_div.childElementCount == 1 && sibling_div.firstElementChild.tagName == "ARTICLE") {
        return sibling_div.firstElementChild;
      } else {
        /* skipping over the View older Posts*/
        sibling_div = sibling_div[sibling_prop];
      }
    }
    console.debug (`get_sibling: couldn't find the next article via parent DIVs.  Current article:`, article);
    return  null;
  } else {
    console.debug (`get_sibling: No article ${sibling_prop} found`);
    return null;
  }
}

/* igkeynav_nav_handler:

   Navigating between posts/articles.
 */
function igkeynav_nav_handler (ev) {
  /* j: view next article (down) (vi controls)
     s: view next article (down) (wasd controls)
     k: view prev article (up) (vi controls)
     w: view prev article (up) (wasd controls) */

  if (article && article.getBoundingClientRect().height == 0) {
    /* our original article is no longer in the document */
    article = null;
  }

  try {
    console.debug (`igkn: igkeynav_nav_handler: ${ev.key} pressed`);
    console.debug (article);

    /* clear border from currently selected one */
    article.style.border = "";
    if ([ "j", "s" ].indexOf (ev.key) >= 0) {
      article = get_sibling (article, true);
    } else if ([ "k", "w" ].indexOf (ev.key) >= 0) {
      article = get_sibling (article, false);
    }
  } catch (e) {
    console.error (e);
    /* general case: hadn't selected an article for the first time */
  }

  const article_sel = "ARTICLE, DIV.x6s0dn4.x78zum5.xdt5ytf.xdj266r.xkrivgy.xat24cr.x1gryazu.x1n2onr6.xh8yej3";

  if (!article) {
    article = document.querySelector (article_sel);
    console.debug (article);
  }

  if (!article) {
    window.alert (`Failed to get article with selector '${article_sel}'`);
    return;
  }

  /* calculate the Y position of the navigated-to article and scroll to it */
  let rect = article.getBoundingClientRect ();
  window.scrollTo (0, rect.top + window.scrollY - 60);  /* -60 for height of Instagram header */

  /* highlight selected article with a red border */
  article.style.border = red_border;
}

/* igkeynav_action_handler:

   Interact with buttons on the screen, to switch between images in a set, or like/bookmark/comment, etc.
 */
function igkeynav_action_handler (ev) {
  console.debug ("igkn: igkeynav_action_handler: article:", article);

  let container;

  const path_part_1 = (window.location.pathname.split ("/")[1] ?? "");
  if (path_part_1 == "stories" || path_part_1 == "p") {
    container = document.body;
  } else if (article) {
    container = article;
  } else {
    console.warn ("igkn: No container found");
    return;
  }

  console.debug ("igkn: igkeynav_action_handler: container:", container);

  console.debug (`igkeynav_action_handler: ${ev.key} pressed`); // `
  if (ev.key in BUTTON_SEL_MAP) {
    let sel = BUTTON_SEL_MAP[ev.key];

    let buttons = [ ... container.querySelectorAll (sel)].filter ((b) => b.getBoundingClientRect ().y > 0);
    if (buttons.length > 1) {
      console.warn (`igkn: button click for ${ev.key}: Uh oh, too many visible buttons matching button selector '${sel}'.  Trying first one:`, buttons[0]);
      buttons[0].click ();
    } else if (buttons.length == 1) {
      console.debug ("igkn: button click for " + ev.key + " on button", buttons[0]);
      buttons[0].click ();
    } else {
      console.debug (`igkn: selector '${sel}' for key '${ev.key}' found no button.`);
    }
    ev.preventDefault ();
    ev.stopPropagation ();
    ev.stopImmediatePropagation ();
    ev.stopBubble = true;
  } else if (ev.key in FOCUS_SEL_MAP) {
    let sel = FOCUS_SEL_MAP[ev.key];

    let elems = [ ... container.querySelectorAll (sel)].filter ((e) => e.getBoundingClientRect ().y > 0);
    if (elems.length > 1) {
      console.warn (`igkn: button click for ${ev.key}: Uh oh, too many visible elements matching focus selector '${sel}'.  Trying first one:`, elems[0]);
      elems[0].focus ();
      ev.preventDefault ();
    } else if (elems.length == 1) {
      console.debug (`igkn: button click for ${ev.key} on elem`, elems[0]);
      elems[0].focus ();
      ev.preventDefault ();
    } else {
      console.debug (`igkn: selector '${sel}' for key '${ev.key}' found no element.`);
    }
  } else if (ev.key in action_map) {
    action_map[ev.key](container);
  }
}

/* igkeynav_handler:

   Handle key events to navigate between posts aka. articles
 */
function igkeynav_handler (ev) {
  if (window.location.pathname.match ("^/direct/.*")) {
    return;
  }

  console.debug ("igkn: igkeynav_handler called");
  // console.log (ev);

  if (ev.target.nodeName == "TEXTAREA" || ev.target.nodeName == "INPUT") {
    return; /* we're typing */
  }

  if (ev.ctrlKey || ev.shiftKey || ev.altKey || ev.metaKey) {
    return; /* none of our combos use a modifier */
  }

  /* j,s: down, k,w: up */
  if ([ "j", "k", "w", "s" ].indexOf (ev.key) >= 0) {
    igkeynav_nav_handler (ev);
  } else {
    igkeynav_action_handler (ev);
  }
}

const action_map = {
  /* map keycodes to functions that take a container (e.g. post ARTCILE, or possibly the whole page), depending on context */
  /* e.g. "h": (container) => { container.style.display = 'none' }, */
};

/* global vars */
const red_border = "2px solid red";
let article = null;

console.log (`igkn: loaded`);
document.addEventListener ("keydown", igkeynav_handler);
